# JSON Web Token (JWT)

## Header
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

## Payload

```json
{
  "sub": "john24",
  "name": "John Doe",
  "iat": 1516239022,
  "claims": "create, edit"
}
```

## Signature

- 256-bit-secret

## Schema Entity

1. User

```json
{
  "id": "Long",
  "name": "String",
  "username": "String",
  "password": "String",
  "roles": []
}
```
2. Role

```json
{
  "id": "Long",
  "name": "String"
}
```

# Authentication and Authorization

# Authentication
- verifies you are who you say you are
- Method:
  - Login form
  - HTTP authentication
  - Custom auth. method

# Authorization
- Decides if you have permission to access a resource
- Method:
  - Access Control URLs
  - Access Control List (ACLs)